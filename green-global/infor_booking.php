<?php
session_start();
ob_start();
	include ("view/include/header.php");
	include ("view/include/connect.php");
  if (isset($_GET['id']) && isset($_SESSION['id']) && $_GET['id']==$_SESSION['id']) {
    $id=$_GET['id'];
  $sql = "SELECT * FROM customer c INNER JOIN booking b ON c.id=b.id_customer
          INNER JOIN room1 r ON b.id_room=r.id
          INNER JOIN room_type t ON r.id_type=t.id 
          WHERE c.id=$id";
  $r=mysqli_query($conn,$sql); 
  //print_r($r);
  if(mysqli_num_rows($r)){
    while($row=mysqli_fetch_array($r)){
      // echo"<pre>";
      // print_r($row);
      // echo"</pre>";
      $name=$row['name'];
      $gender=$row['gender'];
      $address=$row['address'];
      $city=$row['city'];
      $country=$row['country'];
      $email=$row['email'];
      $phone=$row['phone'];
      $name_type=$row['name_type'];
      $id_room[]=$row['id_room'];
      $name_room[]=$row['name_room'];
      $child=$row['child'];
      $people=$row['people'];
      $date_from=date("d-m-Y",strtotime($row['date_from']));
      $date_go=date("d-m-Y",strtotime($row['date_go']));
      $note=$row['note'];
      $id_customer=$row['id_customer'];
      $id_type=$row['id_type'];
    }
  }
  if ($id!=$id_customer)
      header("Location:booking.php");
}
else header("Location:booking.php");
ob_flush();


?>

<!-- <div class="container">
	<div class="main-booking">
    <h3 class="txt-book">THÔNG TIN ĐẶT PHÒNG</h3>
    <div class="row">
      <form>
    	<div class="col-md-12 col-xs-12 book-content">
        <div class="book-infor">
          <div class="row pure-table pure-table-bordered" id="editable" >
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Họ và tên:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $name;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Giới tính:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo ($gender==1)? "Nam" : "Nữ"?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Địa chỉ:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $address;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Thành phố:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $city;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Quốc tịch:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $country;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Email:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $email;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Điện thoại:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $phone;?>
            </div>
          </div>
        </div>
    		<div class="book-infor">
    			<div class="row">
    				<div class="col-md-3 col-xs-4 room">
    					<label for="room_type" ">Loại phòng:</label>
    				</div>
            <div class="col-md-9 col-xs-8 content-text">
            <?php echo $name_type;?>
            </div>
    			</div>
    		</div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Phòng số:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php 
                foreach ($name_room as $room) {
                  echo $room."&nbsp";
                }
              ?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Trẻ em:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $child;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Người lớn:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $people;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Ngày đến:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $date_from;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Ngày đi:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $date_go;?>
            </div>
          </div>
        </div>
        <div class="book-infor">
          <div class="row">
            <div class="col-md-3 col-xs-4 room">
              <label for="room_type" ">Ghi chú:</label>
            </div>
            <div class="col-md-9 col-xs-8 content-text">
              <?php echo $note;?>
            </div>
          </div>
        </div>
    	</div>
          <div class="col-md-12 col-xs-12 button-book">
            <button type="submit" class="btn btn-default btn-book" name="booking1" action="">Đồng ý</button>
            <button type="reset" class="btn btn-default btn-restart">Chỉnh sửa</button> 
          </div>

      </form>
  	</div>
	</div>
  
</div> -->
<div id="edit">
</div>
<div class="container">
<h3>THÔNG TIN ĐẶT PHÒNG</h3>

<div class="row">
  <div  class="col-md-6 col-xs-12">

      <table class="table table-bordered">
        <tr id="" class="pure-table pure-table-bordered editable">
            <th style="width: 50%">Họ và tên:</th>
            <td class="name"><?php echo $name;?></td>
        </tr>
         <tr>
            <th >Giới tính:</th>
            <td>
              <select class="gender">
                <option value="1">Nam</option>
                <option value="2"> <?php if($gender==2)echo "selected='selected'";?>>Nữ</option>
              </select>
            </td>
        </tr>
         <tr id="" class="pure-table pure-table-bordered editable">
            <th style="width: 50%" >Địa chỉ:</th>
            <td class="address"><?php echo $address;?></td>
        </tr>
         <tr id="" class="pure-table pure-table-bordered editable">
            <th style="width: 50%" >Thành phố:</th>
            <td class="city"><?php echo $city;?></td>
        </tr>
         <tr>
            <th style="width: 50%" >Quốc tịch:</th>
             <td>
              <select class="country">
                <option>Việt Nam</option>
                <option> <?php if($country=='anh')echo "selected='selected'";?>>Anh</option>
              </select>
            </td>
        </tr>
         <tr id="" class="pure-table pure-table-bordered editable">
            <th style="width: 50%" >Email:</th>
            <td class="email"><?php echo $email;?></td>
        </tr>
         <tr id="" class="pure-table pure-table-bordered editable">
            <th style="width: 50%" >Điện thoại:</th>
            <td class="phone"><?php echo $phone;?></td>
        </tr>
      </table>
</div>
    <div  class="col-md-6 col-xs-12">
      <table class="table table-bordered">
        <tr>
            <th style="width: 50%" >Loại phòng:</th>
            <td>
            <select class="room_type">
            <?php
                  $sql2="SELECT id,name_type FROM room_type";
                  $r=mysqli_query($conn,$sql2);
                  $i=1;
                  if($num=mysqli_num_rows($r)){
                    while($row=mysqli_fetch_array($r)){
                      echo"<option value='{$row['id']}'";
                      if($id_type==$i) echo "selected='selected'";
                        echo ">".$row['name_type']."</option>";
                      $i++;
                    }
                  }
                
            ?>
            </select>
            </td>
        </tr>
         <tr>
            <th style="width: 50%" >Phòng số:</th>
            <td class="num_room">
            <!-- <input id="plane" value="<?php echo $id_type ?>" hidden="hidden" /> -->
            <input id="id_last" value="<?php echo $id ?>" hidden="hidden" />
            <!-- <div id="hide" style="display: inline;">
            <?php 
            $i=0;
              foreach ($id_room as $value) {
                echo"<input type='checkbox' name='num_room[]'  value='$value' "."checked >".$name_room[$i]."&nbsp";
                $i++;
              }
            ?>
              </div> -->
              <div id="num_room" style="display: inline;">
                  <?php 
            $i=0;
              foreach ($id_room as $value) {
                echo"<input type='checkbox' name='num_room[]'  value='$value' "."checked >".$name_room[$i]."&nbsp";
                $i++;
              }
            ?>
              </div>
              <?php foreach($id_room as $value) {?>
              <input class="plane1" value="<?php echo $value; ?>" name="<?php echo $value;?>" hidden="hidden" />
              <?php }?>
            </td>
        </tr>
         <tr id="" class="pure-table pure-table-bordered editable">
            <th style="width: 50%" >Trẻ em:</th>
            <td class="child"><?php echo $child;?></td>
        </tr>
         <tr id="" class="pure-table pure-table-bordered editable">
            <th style="width: 50%" >Người lớn:</th>
            <td class="people"><?php echo $people;?></td>
        </tr>
         <tr>
            <th style="width: 50%" >Ngày đến:</th>
            <td class="">
              <div class="datepicker date" data-date-format="dd-mm-yyyy">
                <input type="text" name="date_from" class="input-group-addon date_from" style="padding: 0;height: 100%;margin: 0" readonly="" value="<?php echo $date_from; ?>"/>
              </div>
            </td>
        </tr>
         <tr>
            <th style="width: 50%" >Ngày đi:</th>
            <td class="">
              <div class="datepicker date" data-date-format="dd-mm-yyyy">
                <input type="text" name="date_go" style="padding: 0;height: 100%;margin: 0" class="input-group-addon date_go" readonly="" value="<?php echo $date_go; ?>"/>
              </div>
            </td>
        </tr>
         <tr id="" class="pure-table pure-table-bordered editable">
            <th style="width: 50%" >Ghi chú:</th>
            <td class="note"><?php echo $note;?></td>
        </tr>
      </table>
      
    </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-xs-12" id="" style="text-align: center">
         <button type="submit" class="btn btn-default" id="accept" name="booking1">Đồng ý</button>
      </div>
    </div>
</div>
<?php include ("view/include/footer.php"); ?>
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/bookinginfo-ajax.js"></script>
<script type="text/javascript" src="js/edit_booking-ajax.js"></script>
<script type="text/javascript" src="js/mindmup-editabletable.js"></script>

<script>
  $('.editable').editableTableWidget();
   $('#editable td.uneditable').on('change', function(evt, newValue) {
    return false;
 }); 
</script>
<script >
 $(function () {  
            $(".datepicker").datepicker({         
                autoclose: true,         
                todayHighlight: true 
            });
        });
</script>
