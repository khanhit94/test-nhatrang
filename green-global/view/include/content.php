<div class="slide">
	<img src="images/Home_20141124-fix.png" class="main-slide img-responsive"> 
    <div class="sm-image">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-3">
                    <img src="images/slide1.png" class="img-responsive">
                </div>
                <div class="col-md-3 col-xs-3">
                    <img src="images/slide2.png" class="img-responsive">
                </div>
                <div class="col-md-3 col-xs-3">
                    <img src="images/slide3.png" class="img-responsive">
                </div>
                <div class="col-md-3 col-xs-3">
                    <img src="images/slide4.png" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
	<div class="link">
		<div class="row">
			<div class="col-md-1 col-xs-0 col-sm-1"></div>
			<div class="col-md-2 col-xs-4 col-sm-2">
				<p class="img-link first"><a href=""><img src="images/facebook.png"></a></p>
			</div>
			<div class="col-md-2 col-xs-4 col-sm-2">
				<p class="img-link"><a href=""><img src="images/google.png"></a></p>
			</div>
			<div class="col-md-2 col-xs-4 col-sm-2">
				<p class="img-link"><a href=""><img src="images/youtube.png"></a></p>
			</div>
			<div class="col-md-2 col-xs-6 col-sm-2">
				<p class="img-link"><a href=""><img src="images/tripadvisor.png"></a></p>
			</div>
			<div class="col-md-2 col-xs-5 col-sm-2">
				<p class="img-link"><a href=""><img src="images/virtual.png"></a></p>
			</div>
			<div class="col-md-1 col-xs-1 col-sm-1"></div>
		</div>
	</div>
</div>