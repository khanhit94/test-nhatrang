<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  	<meta http-equive="X-UA-Compatible" content="IE=edge">	
  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  	<title>bootstrap</title>
  	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
  	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style_fix.css">
    <?php $url=$_SERVER['PHP_SELF'] ;
        if ($url=='/green-global/booking.php') { ?>
            <link rel="stylesheet" type="text/css" href="css/booking.css">
    <?php }?>
    <?php $url=$_SERVER['PHP_SELF'] ;
        if ($url=='/green-global/infor_booking.php') { ?>
           <link rel="stylesheet" type="text/css" href="css/datepicker.css">
    <?php }?>
    <!-- Datepicker-->
</head>
<body>
    <div id="header">
        <div class="top-menu">
            <div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-2 "></div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<div id="search">
							<form>
								<input type="text" name="search" placeholder="SEARCH">
							</form>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-7">
						<div id="language">
							<select id="english">
								<option>ENGLISH</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
    
    <nav class="navbar navbar-inverse navbar-static-top">
		<div class="container">
        	<div class="navbar-header">
            	<div class="container">
            		<div class="row">
            			<div class="col-xs-2">
                        	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                            	<span class="sr-only">Toggle navigation</span>
                            	<span class="icon-bar"></span>
                            	<span class="icon-bar"></span>
                            	<span class="icon-bar"></span>
                        	</button>
                        </div>
            			<div class="col-xs-2"></div>
             			<div class="col-xs-4">
             				<div id="sm-logo">              
                        		<img src="images/sm-icon.png" class="navbar-brand navbar-right">
                        	</div>
                        </div>
                        <div class="col-xs-4">
                        	<div class="btn btn-primary booking1">Booking
                        	</div>
                        </div>       
                    </div>
                </div> 
            </div><!--end .navbar-header-->
 			<div class="navbar-collapse collapse" id="menu">
                <div id="img1"><img src="images/logo.png"></div>
                <ul class="nav navbar-nav">
                    <li><a href="">Trang chủ</a></li>
                    <li><a href="">Giới thiệu</a></li>
                    <li><a href="">Loại phòng</a></li>
                    <li><a href="">Dịch vụ</a></li>
                    <li><a href="">Nhà hàng-Bar</a></li>
                    <li class="hiden1"><a href="">Hình ảnh</a></li>
                    <li class="hiden1"><a href="">Tin tức</a></li>
                    <li class="hiden"><a href="">Tuyển dụng</a></li>
                    <li class="hiden"><a href="">Liên hệ</a></li>
                </ul>
            </div>
        </div><!-- end .container-->
 	</nav>
    <div class="content">
        <div class="container-fluid">