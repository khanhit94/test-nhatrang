<div class="footer">
    <div class="row">
        <div class="col-md-1 col-sm-1"></div>
        <div class="col-md-2 col-xs-4 col-sm-12 hotel">
            <div class="sub-title">
                <a href=""><h3>Hotel</h3></a>
                <div class="submenu">
                    <ul class="ht-menu">
                        <li><a href="">Booking</a></li>
                        <li><a href="">About Us</a></li>
                        <li><a href="">Location</a></li>
                        <li><a href="">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-xs-5 col-sm-12 hotel">
            <div class="sub-title">
                <a href=""><h3>Accommodation</h3></a>
                <div class="submenu">
                    <ul>
                        <li><a href="">Room</a></li>
                        <li><a href="">Equipment</a></li>
                        <li><a href="">Services</a></li>
                        <li><a href="">Safety</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-xs-3 col-sm-12 hotel">
            <div class="sub-title">
                <a href=""><h3>Services</h3></a>
                <div class="submenu">
                    <ul>
                        <li><a href="">Accommodation</a></li>
                        <li><a href="">Conferences</a></li>
                        <li><a href="">Gastronomy</a></li>
                        <li><a href="">Relaxation</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-xs-4 col-sm-12 hotel">
            <div class="sub-title">
                <a href=""><h3>Menu</h3></a>
                <div class="submenu">
                    <ul>
                        <li><a href="">Menu restautant Promient</a></li>
                        <li><a href="">Bussiness menu</a></li>
                        <li><a href="">Lopby bar menu</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-xs-8 col-sm-12 hotel">
            <div class="sub-title">
                <a href=""><h3>Contact us</h3></a>
                <div class="submenu">
                    <ul>
                        <li><a href="">15B Nguyễn Thiện Thuật, Nha Trang</a></li>
                        <li><a href="">Tell: +84.510.713176</a></li>
                        <li><a href="">Fax: +84.510.33333333</a></li>
                        <li><a href="">Location Google map</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-xs-0 col-sm-1"></div>
    </div>
        <div class="copyright">
            <div class="row">
                <div class="col-md-1 col-xs-1 col-sm-1"></div>
                <div class="col-md-4 col-xs-12 col-sm-12">
                    <span>Copyright &copy 2013 Vitour-All Right Reseved </span>
                </div>
                <div class="col-md-4 col-xs-12 col-sm-12">
                    <div class="paypal">
                        <img src="images/paypal.png">
                        <img src="images/visa.png">
                        <img src="images/express.png">
                        <img src="images/master-card.png">
                    </div>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-12">
                    <div class="letter">
                        <p>Leave a Message</p>
                    </div>
                </div>
                </div>
        </div>
</div>
</div>  
</div>
</div>
</body>
</html>