<?php 
  session_start();
  include ("view/include/header.php"); 
  include ("view/include/booking_form.php");
  include ("validate.php");
  include ("view/include/connect.php");
  $data['child'] = isset($_POST['child']) ? $_POST['child'] : ''; 
  $data['people'] = isset($_POST['people']) ? $_POST['people'] : ''; 
  $data['date_from'] = isset($_POST['date_from']) ? $_POST['date_from'] : ''; 
  $data['date_go'] = isset($_POST['date_go']) ? $_POST['date_go'] : '';
  $data1['note'] = isset($_POST['note']) ? mysqli_real_escape_string($conn,strip_tags($_POST['note'])):''; 
  $data['name'] = isset($_POST['name']) ? mysqli_real_escape_string($conn,strip_tags($_POST['name'])):''; 
  $data['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
  $data['country'] = isset($_POST['country']) ? $_POST['country'] : '';
  $data['address'] = isset($_POST['address']) ? mysqli_real_escape_string($conn,strip_tags($_POST['address'])):''; 
  $data['city'] = isset($_POST['city']) ? mysqli_real_escape_string($conn,strip_tags($_POST['city'])):''; 
  $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
  $data['phone'] = isset($_POST['phone']) ? $_POST['phone'] : ''; 
  $data1['room_type'] = isset($_POST['room_type']) ? $_POST['room_type']:''; 
  $data['num_room'] = isset($_POST['num_room']) ? $_POST['num_room']:'';
  $error=array();
  $error1=array();
  if (isset($_POST['booking1'])) {
    $error1=check_select($data1); 
    $error=check_empty($data);
    $date_from= date("Y-m-d",strtotime($data['date_from']));
    $date_go= date("Y-m-d",strtotime($data['date_go']));
    if($error==null && $error1==null ){
      $sql = "INSERT INTO customer (name,gender,address,city,country,email,phone)
              VALUES ('{$data['name']}','{$data['gender']}','{$data['address']}','{$data['city']}','{$data['country']}','{$data['email']}','{$data['phone']}') " ;
      if (check_conn($sql)) {
        $last_id = mysqli_insert_id($conn);
        } else 
        {
        exit($message="Đặt phòng không thành công");
        }
      foreach ($data['num_room'] as $key => $num_room) {
        $sql1= "INSERT INTO booking (id_customer,id_room,child,people,date_from,date_go,note)
                VALUES ('$last_id','{$num_room}','{$data['child']}','{$data['people']}','$date_from','$date_go','{$data1['note']}') " ;
        if (mysqli_query($conn, $sql1)) {
          $message="Đặt phòng thành công";
        } else 
        {
          $sql="DELETE FROM customer WHERE id=$last_id";
          mysql_query($conn,$sql);
          exit($message="Đặt phòng không thành công");
        }
        $sql2= "UPDATE room1 SET status=1 WHERE id='{$num_room}'";
        if (mysqli_query($conn, $sql2)) {
          $message="Đặt phòng thành công";
        } else 
        {
          exit($message="Đặt phòng không thành công");
        }
      }
    mysqli_close($conn);
     $_SESSION['id'] = $last_id;
    }

  }
?>
<div class="container">
	<div class="main-booking">
    <h3 class="txt-book">ĐẶT PHÒNG</h3> 
    <?php 
    if (isset($message)) {
      global $last_id;
      if ($message=="Đặt phòng thành công") { ?>
      <input id="plane" value="<?php echo $last_id ?>" hidden="hidden" />
      <script type='text/javascript'>
        var last_id = document.getElementById('plane').value
        if(confirm('Đặt phòng thành công.Bạn có muốn xem thông tin?'))
          window.location="infor_booking.php?id="+last_id;
        else window.location='booking.php';
      </script>;
    <?php 
      } 
    }
    ?>
    <div class="row">
    	<form method="post" action="">
    		<div class="col-md-6 col-xs-12" >
    			<div class="book-info">
    				<h3>Booking information</h3>
    				<div class="row">
    					<div class="col-md-3 col-xs-4 room">
    						<label for="room_type" ">Loại phòng:</label><span>*</span>
    					</div>
    					<div class="col-md-9 col-xs-8 ">
    						<select class="ip-text room_type" name="room_type">
                 <option value="title">Chọn loại phòng</option> 
                  <?php 
                  $sql2="SELECT id,name_type FROM room_type";
                  $r=mysqli_query($conn,$sql2);
                  if(mysqli_num_rows($r)){
                    while($row=mysqli_fetch_array($r)){
                      echo"<option value='{$row['id']}'";
                      if(isset($data1['room_type'])&&$data1['room_type']==$row['id']) echo"selected='selected'";
                        echo ">".$row['name_type']."</option>";
                    }
                  }
                  ?>
                </select>
    					</div> 
              <div class="error"><?php echo (isset($error1['room_type'])) ? "Chọn loại phòng": ''?></div>
    				</div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Phòng số:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 " id="num_room">
              <!-- <input type="checkbox" name="">c -->
                <!-- <select class="ip-text" id= "num_room" name="num_room[]"" multiple>
                 <option>Chọn phòng</option> 
                </select> -->

              </div> 
              <div class="error"><?php echo (isset($error['num_room'])) ? "Chọn số phòng" :''?></div>
            </div>
          </div>
      		<div class="book-info">
      			<div class="row">
      		    <div class="col-md-3 col-xs-4 room">
      				  <label for="child">Trẻ em:</label><span>*</span>
      			  </div>
      			  <div class="col-md-9 col-xs-8">
        			  <div class="row">
        				  <div class="col-md-3 col-xs-3 child">
        					  <input type="text" name="child" class="ip-text" value="<?php echo (isset($data['child']))? $data['child'] :'';?>" >
        				  </div>  
                  <div class="col-md-9 col-xs-9 people">
                    <div class="row">
                      <div class="col-md-2 col-xs-0"></div>
                      <div class="col-md-4 col-xs-6 ">
                        <label for="child">Người lớn:</label><span>*</span>
                      </div>
                      <div class="col-md-6 col-xs-6 people1">
                        <input type="text" name="people" class="ip-text" value="<?php echo (isset($data['people']))? $data['people'] :'';?>" > 
                      </div>
                    </div>
    			        </div>
  				      </div>	
                <div class="error error2">
                <?php
                if (isset($_POST['booking1'])) {
                  if(isset($error['child'])) echo "Số lượng trẻ em không được trống";
                  else if (isset($error['people'])) echo "Số lượng người lớn không được trống";
                  else if (!is_num($data['child'])) {
                    $error['child']='child';
                    echo "Số lượng phải là số";
                  }
                  else if (!is_num($data['people'])) {
                    $error['people']='people';
                    echo "Số lượng phải là số";
                  }
                }
                ?>
                </div>
			        </div> 
  		      </div>
          </div>
      		<div class="book-info">
          	<div class="row">
          		<div class="col-md-3 col-xs-4 room ">
          			<label for="date_from">Ngày đến:</label><span>*</span>
          		</div>
          		<div class="col-md-9 col-xs-8 gone">
                <input type="text" name="" class="ip-text" readonly="readonly">
                  <div class=" datepicker date" data-date-format="dd-mm-yyyy">
          					<input type="text" name="date_from" class="input-group-addon" readonly="" value="<?php if(isset($data['date_from'])) echo $data['date_from'];?>"/>
                  </div>
          		</div>
              <div class="error">
              <?php  
              if(isset($error['date_from'])) "Ngày đến không được trống";
              elseif (strtotime($data['date_from']) > strtotime($data['date_go'])) {
                echo "Ngày đến phải nhỏ hơn ngày đi";
                $error['date']='date';
              }
              ?>
              </div>
          	</div>
      		</div>  
          <div class="book-info">
          	<div class="row">
          		<div class="col-md-3 col-xs-4 room">
          			<label for="date_go">Ngày đi:</label><span>*</span>
          		</div>
          		<div class="col-md-9 col-xs-8 gone">
          			<input type="text" name="" class="ip-text" readonly="readonly">
                  <div class=" datepicker date" data-date-format="dd-mm-yyyy">
                    <input type="text" name="date_go" class="input-group-addon" readonly="" value="<?php if(isset($data['date_go'])) echo $data['date_go'];?>">
                  </div>
          		</div>
              <div class="error"><?php  echo (isset($error['date_go']))? "Ngày đi không được trống" :'';?></div>
          	</div>
          		</div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="date_go">Ghi chú:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8">
                <textarea rows="4" maxlength="100" name="note" style="width: 100%;" value=""><?php if(isset($data1['note'])) echo $data1['note'];?></textarea>
              </div>
            </div>
          </div>	
		    </div>
		    <div class="col-md-6 col-xs-12" >
			    <div class="book-info">
            <h3>Thông tin cá nhân</h3>
              <div class="row">
                <div class="col-md-3 col-xs-4 room">
                  <label for="room_type" ">Họ và tên:</label><span>*</span>
                </div>
                <div class="col-md-9 col-xs-8 ">
                  <input type="text" name="name" class="ip-text" value="<?php if(isset($data['name'])) echo $data['name'];?>">
                </div>
                <div class="error"><?php  echo (isset($error['name']))? "Họ và tên không được trống" :'';?></div>
              </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Giới tính:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                <select class="ip-text" name="gender">
                  <option value=1>Nam</option>
                  <option value=2>Nữ</option>
                </select>
              </div>
              <div class="error"></div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Địa chỉ:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                <input type="text" name="address" class="ip-text" value="<?php if(isset($data['address'])) echo $data['address'];?>">
              </div>
              <div class="error"><?php  echo (isset($error['address']))? "Địa chỉ không được trống" :'';?></div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Thành phố:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                <input type="text" name="city" class="ip-text" value="<?php if(isset($data['city'])) echo $data['city'];?>">
              </div>
              <div class="error"><?php  echo (isset($error['city']))? "Thành phố không được trống" :'';?></div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Quốc tịch:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                <select class="ip-text" name="country">
                  <option value="việt nam">Việt Nam</option>
                  <option value="anh">Anh</option>
                </select>
              </div>
              <div class="error"></div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Email:</label><span>*</span>
                  
              </div>
              <div class="col-md-9 col-xs-8 ">
                <input type="text" class="ip-text" name="email" value="<?php if(isset($data['email'])) echo $data['email'];?>">     
              </div>
              <div class="error">
              <?php 
              if (isset($error['email'])) echo "Email không được trống" ; 
              else if (isset($_POST['booking1'])) {
                  if (!is_email($data['email'])) echo "Email không đúng định dạng";
              }
              ?>
              </div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Điện thoại:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                <input type="text" name="phone" class="ip-text" value="<?php if(isset($data['phone'])) echo $data['phone'];?>">
              </div>
              <div class="error">
              <?php 
              if (isset($error['phone'])) echo "Số điện thoại không được trống";
              else if (isset($_POST['booking1'])) {
                if (!is_phone($data['phone'])) echo "Số điện thoại không đúng";
              }
              ?>
              </div>
            </div>
          </div>
		    </div>
        <div class="row">
          <div class="col-md-12 col-xs-12 button-book">
            <button type="submit" class="btn btn-default btn-book" name="booking1" action=""onclick="">Đặt phòng</button>
            <button type="reset" class="btn btn-default btn-restart">Nhập lại</button> 
          </div>
        </div>
		  </form>
    </div>
  </div>
</div>
<?php include ("view/include/footer.php"); ?>
<?php include ("view/include/javascip.php");?>

